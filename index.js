const express = require("express");
const path = require("path");

const app = express();

const port = 8000;

app.use(express.static(__dirname + "/view"));

app.use("/",(req,res,next)=>{
    console.log("Date: "+ new Date());
    next();
});

app.use("/",(req,res,next)=>{
    console.log("Url: "+ req.get("host"));
    next();
});

app.get("/",(req, res)=>{
    console.log(__dirname);
    return res.sendFile(path.join(__dirname + "/view/login.html"));
});

app.listen(port,()=>{
    console.log("Connect to port: "+port);
})